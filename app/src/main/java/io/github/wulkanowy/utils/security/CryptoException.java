package io.github.wulkanowy.utils.security;


public class CryptoException extends Exception {

    public CryptoException(String message) {
        super(message);
    }
}
